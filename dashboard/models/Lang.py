from django.db import models


class Lang(models.Model):
    name = models.CharField(max_length=20, default="")
    code = models.CharField(max_length=6, blank=True)

    class Meta:
        ordering = ("code", "name")
