from django.db import models


class Menu(models.Model):
    title = models.CharField(max_length=20, blank=True, default='')
    url = models.CharField(max_length=100, blank=False, default='/')
    parent_id = models.IntegerField(default=0)
    component = models.CharField(max_length=100, blank=True)
    icon_type = models.CharField(max_length=100, blank=True)
    breadcrumb = models.CharField(max_length=100, blank=True)
    description = models.CharField(max_length=100, blank=True)
    order = models.IntegerField(default=1)

    def __str__(self):
        """
            toString function
        :return: str
        """

        return self.title

    class Meta:
        ordering = ('order',)
