from rest_framework import serializers
from ..models import Menu


# Controller or Serializer
class MenuSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Menu  # Entidad para cargar en la vista
        fields = ('id', 'url', 'title', 'parent_id', 'component', 'icon_type', 'breadcrumb',
                  'order')  # Campos a representar en la respusta JSON
