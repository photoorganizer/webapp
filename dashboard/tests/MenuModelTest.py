from django.test import TestCase
from ..models import Menu


class MenuModelTest(TestCase):
    """ Test module for Menu model """

    def setUp(self):
        Menu.objects.create(
            title='Home', order=1, icon_type='home', url='/')
        Menu.objects.create(
            title='Profile', order=3, icon_type='user', url='/profile')

    def test_get_by_title(self):
        menu_home = Menu.objects.get(title='Home')
        menu_profile = Menu.objects.get(title='Profile')
        self.assertEqual(menu_home.title, 'Home')
        self.assertEqual(menu_profile.title, 'Profile')
