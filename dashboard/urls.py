from django.urls import path, include
from rest_framework import routers

from .views import MenuViewSet

app_name = 'dashboard'

router = routers.DefaultRouter()
router.register(r'menus', MenuViewSet)

urlpatterns = []
