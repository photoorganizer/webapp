from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from ..models import Menu
from ..serializers import MenuSerializer


# View
class MenuViewSet(viewsets.ModelViewSet):
    queryset = Menu.objects.filter()  # Dataset para gestionar la respuesta http "Lista de menus"
    serializer_class = MenuSerializer  # Campos a mapear en la respuesta

    authentication_classes = (TokenAuthentication,)  # TIpo de autentciacion que utiliza la vista
    permission_classes = (IsAuthenticated,)  # Permisos que necesita la vista

