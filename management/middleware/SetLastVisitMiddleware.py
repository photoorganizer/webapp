from django.contrib.auth.models import User
from django.utils.timezone import now


class SetLastVisitMiddleware(object):
    """
        Update last_login field
    """

    def process_response(self, request, response):
        """
        Process response to update last_login field.
        :param request:
        :param response:
        :return:
        """
        if request.user.is_authenticated():
            # Update last visit time after request finished processing.
            User.objects.filter(pk=request.user.pk).update(last_visit=now())
        return response
