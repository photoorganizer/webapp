from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from photo.models import Album, Category


class UserSerializer(serializers.ModelSerializer):
    username = serializers.CharField(required=True,
                                     validators=[UniqueValidator(queryset=User.objects.all(),
                                                                 message='Nombre de usuario duplicado.')])
    email = serializers.CharField(required=True,
                                  validators=[UniqueValidator(queryset=User.objects.all(),
                                                              message='Email duplicado')])
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = serializers.CharField(
        write_only=True,
        required=True,
        help_text='Leave empty if no change needed'
    )

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name', 'password', 'get_full_name')

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)

        album_name = "{}'s Photos".format(validated_data['username'].capitalize())
        user.set_password(validated_data['password'])

        print(user.save())

        album, create = Album.objects.get_or_create(title=album_name, owner=user)
        category, status = Category.objects.get_or_create(name='Default')

        print(category)
        album.categories.add(category)
        album.save()

        return user
