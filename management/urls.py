from rest_framework import routers
from .views import UserViewSet

app_name = 'management'

router = routers.SimpleRouter()
router.register(r'users', UserViewSet)