from django.contrib.auth.models import User
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import mixins, viewsets, status

from management.serializers import UserSerializer
from photo.models import Album


class UserViewSet(mixins.RetrieveModelMixin, mixins.ListModelMixin, mixins.CreateModelMixin, viewsets.ViewSet):
    """
        View for data from User Model
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_permissions(self):
        if self.request.method == 'POST':
            return [permission() for permission in (AllowAny,)]
        return super(UserViewSet, self).get_permissions()

    def retrieve(self, request, pk=None, **kwargs):
        """
            If provided 'pk' is "me" then return the current user.
        """
        if request.user and pk == 'me':
            return Response(UserSerializer(request.user).data)
        return super(UserViewSet, self).retrieve(request, pk)

    def list(self, request, *args, **kwargs):
        return Response(UserSerializer(self.queryset, many=True).data)

    def create(self, request, *args, **kwargs):
        serialized = UserSerializer(data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=status.HTTP_201_CREATED)
        return Response(serialized.errors, status=status.HTTP_409_CONFLICT)
