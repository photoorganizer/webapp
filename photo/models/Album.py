from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords


class Album(models.Model):
    """

    """
    title = models.CharField(max_length=255, blank=False, default='New album')
    owner = models.ForeignKey(User, related_name='albums', on_delete=models.DO_NOTHING)
    categories = models.ManyToManyField('Category', related_name='albums')
    description = models.TextField(max_length=300, blank=True, default='Default album')
    order = models.IntegerField(default=0)
    thumbnail = models.CharField(max_length=150, blank=True, default='default.jpg')
    history = HistoricalRecords()

    def __str__(self):
        """

        :return:
        """
        return '<Album {}>'.format(self.title)

    class Meta:
        ordering = ('order',)
