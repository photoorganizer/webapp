from django.db import models
from simple_history.models import HistoricalRecords


class Category(models.Model):
    name = models.CharField(max_length=150, blank=True)
    tags = models.ManyToManyField('Tag', related_name='categories')
    created_at = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return '<Category {}>'.format(self.name)

    class Meta:
        ordering = ('name',)
