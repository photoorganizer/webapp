from django.db import models
from simple_history.models import HistoricalRecords


class Detection(models.Model):
    tag = models.ForeignKey('Tag', related_name='detections', on_delete=models.DO_NOTHING, null=True)
    probability = models.FloatField(default=0)
    box_points = models.TextField()
    picture = models.ForeignKey('Picture', related_name='detections', on_delete=models.DO_NOTHING, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return '<Detection {}>'.format(self.name)

    class Meta:
        ordering = ('created_at',)
