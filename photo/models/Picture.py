from django.db import models
from django.contrib.auth.models import User
from simple_history.models import HistoricalRecords

from photo.models.Category import Category
from photo.models.Tag import Tag
from photo.utils import PicturePrediction, PictureObjectDetection




class Picture(models.Model):
    name = models.CharField(max_length=200, blank=False)
    url = models.CharField(max_length=255, blank=True)
    image = models.ImageField(upload_to='media/', blank=True)
    size = models.IntegerField(default=0)  # BYTES
    content_type = models.CharField(max_length=50, blank=True)
    owner = models.ForeignKey(User, related_name='photos', null=True, on_delete=models.DO_NOTHING)
    status = models.CharField(max_length=50, blank=True, default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    def __str__(self):
        return '<Picture {}>'.format(self.name)

    class Meta:
        ordering = ('created_at',)

    def make_predictions(self):
        """
        Analyze current picture. Store predictions in database.
        """
        results = PicturePrediction().analyze_single_image(self)
        DEFAULT_CATEGORY, status = Category.objects.get_or_create(name='Default')

        if results:
            for key, val in results.items():
                tag, created  = Tag.objects.get_or_create(name=key)
                current_tag = tag
                try:
                    DEFAULT_CATEGORY.tags.add(tag)
                except Exception as err:
                    pass
                self.predictions.create(tag=current_tag, probability=float(val))

    def make_detections(self):
        """
         Analyze current picture. Store predictions in database.
        :return:
        """
        results = PictureObjectDetection().detect_single_image(self)

        if results:
            for key, val in results.items():
                predict = self.predictions.create(name=key, probability=float(val))
                predict.save()
