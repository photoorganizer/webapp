from django.db import models
from simple_history.models import HistoricalRecords


class Prediction(models.Model):
    tag = models.ForeignKey('Tag', related_name='predictions', on_delete=models.DO_NOTHING, null=True)
    probability = models.FloatField(default=0)
    picture = models.ForeignKey('Picture', related_name='predictions', on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return '<Prediction {}>'.format(self.tag.name)

    class Meta:
        ordering = ('created_at',)
