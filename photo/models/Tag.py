from django.db import models
from simple_history.models import HistoricalRecords


class Tag(models.Model):
    name = models.CharField(max_length=150, default='', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords()

    def __str__(self):
        return '<Tag {}>'.format(self.name)

    class Meta:
        ordering = ('name',)
