from .Album import Album
from .Picture import Picture
from .Prediction import Prediction
from .Detection import Detection
from .Category import Category
from .Tag import Tag
