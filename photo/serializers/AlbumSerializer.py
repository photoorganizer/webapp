from rest_framework import serializers
from photo.models import Album, Picture
from photo.serializers.PictureSerializer import PictureSerializer
from photo.serializers.CategorySerializer import CategorySerializer


# Controller or Serializer


class AlbumSerializer(serializers.ModelSerializer):
    categories = CategorySerializer(many=True)
    photos = serializers.SerializerMethodField()

    class Meta:
        model = Album  # Entidad para cargar en la vista
        fields = (
            'id', 'title', 'description', 'thumbnail', 'categories',
            'photos')  # Campos a representar en la respusta JSON

    def get_photos(self, album):
        tags = []

        for category in album.categories.all():
            for tag in category.tags.all():
                tags.append(tag)

        photos = Picture.objects.filter(predictions__tag__in=tags, owner=album.owner).distinct()

        if photos:
            return PictureSerializer(instance=photos, many=True).data
        return []
