from rest_framework import serializers

from ..serializers.TagSerializer import TagSerializer
from ..models import Category


class CategorySerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('name', 'tags')
