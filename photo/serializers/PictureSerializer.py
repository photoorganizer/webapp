from rest_framework import serializers

from photo.serializers.PredictionSerializer import PredictionSerializer
from photo.serializers.TagSerializer import TagSerializer
from ..models import Picture
from ..utils import image_as_base64


class PictureSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField()
    uid = serializers.CharField(source='id', read_only=True)
    predictions = PredictionSerializer(many=True, read_only=True)

    class Meta:
        model = Picture
        fields = ('id', 'uid', 'name', 'content_type', 'url', 'predictions')

    def get_url(self, picture):
        return image_as_base64(picture, picture.content_type)
