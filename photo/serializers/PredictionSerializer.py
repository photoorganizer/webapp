from rest_framework import serializers

from ..serializers.TagSerializer import TagSerializer
from ..models import Prediction


class PredictionSerializer(serializers.ModelSerializer):
    tag = TagSerializer(read_only=True)

    class Meta:
        model = Prediction
        fields = ('tag',)
