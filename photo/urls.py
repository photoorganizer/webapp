from rest_framework import routers

from .views import AlbumViewSet, PictureViewSet

app_name = 'photo'

router = routers.SimpleRouter()
router.register(r'albums', AlbumViewSet)
router.register(r'pictures', PictureViewSet)

urlpatterns = []
