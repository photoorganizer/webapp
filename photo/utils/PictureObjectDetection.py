import os
from datetime import datetime
from django.conf import settings
from imageai.Detection import ObjectDetection

ROOT_MODELS = os.path.join(settings.BASE_DIR, 'media/models')
ROOT_PICTURES_OUT = os.path.join(settings.BASE_DIR, 'media/pictures/out')


class PictureObjectDetection(object):
    """

    """
    def __init__(self):
        self.detector = ObjectDetection()
        self.__set_and_load_model()

    def __set_and_load_model(self):
        """

        :return:
        """
        self.detector.setModelTypeAsYOLOv3()
        self.detector.setModelPath(os.path.join(ROOT_MODELS, 'yolo.h5'))
        self.detector.loadModel()

    def detect_single_image(self, __picture):
        """

        :param __picture:
        :return:
        """
        file_name = 'picture_{}_{}.jpg'.format(__picture.owner.id, datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))
        out_img = os.path.join(ROOT_PICTURES_OUT, file_name)

        detections = self.detector.detectObjectsFromImage(input_image=__picture.url, output_image_path=out_img,
                                                          minimum_percentage_probability=30)
        return detections
