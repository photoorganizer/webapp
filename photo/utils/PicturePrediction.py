import os
from django.conf import settings
from imageai.Prediction import ImagePrediction

ROOT_MODELS = os.path.join(settings.BASE_DIR, 'media/models')


class PicturePrediction(object):

    def __init__(self):
        self.prediction = ImagePrediction()
        self.__set_and_load_model()

    def __set_and_load_model(self):
        self.prediction.setModelTypeAsDenseNet()
        self.prediction.setModelPath(os.path.join(ROOT_MODELS, 'DenseNet-BC-121-32.h5'))
        self.prediction.loadModel()

    def analyze_single_image(self, __picture, __results=5):
        predictions, probabilities = self.prediction.predictImage(__picture.url, result_count=__results)
        return dict(zip(predictions, probabilities))

    # TODO: Create method to analyze multiple files
