import base64
import os
from datetime import datetime
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings


def get_file_name():
    return 'picture_{}.jpg'.format(datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))


def save_file(file):
    """
    Save file in MEDIA directory

    :param file:
    :return: url
    """
    content = ContentFile(file.read())
    path = default_storage.save(get_file_name(), content)

    return os.path.join(settings.MEDIA_ROOT, path)


def image_as_base64(picture, img_format='image/png'):
    """
    Transform image to base64 blob
    :param picture: Picture entry
    :param img_format:
    """
    if not os.path.isfile(picture.url):
        return None

    encoded_string = ''
    with open(picture.url, 'rb') as img_f:
        encoded_string = base64.b64encode(img_f.read())

    return 'data:{};base64,{}'.format(picture.content_type, encoded_string.decode('utf8'))