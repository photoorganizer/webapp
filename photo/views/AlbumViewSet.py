from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..models import Album
from ..serializers import AlbumSerializer


# View
class AlbumViewSet(viewsets.ModelViewSet):
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def list(self, request, **kwargs):
        albums = request.user.albums.all()
        albums_response = []
        if albums:
            serializer = AlbumSerializer(albums, many=True)
            albums_response = [data for data in serializer.data if
                               len(data.get('photos', None)) > 0 or data.get('title', None).split("'")[
                                   0].lower() == request.user.username]
            return Response(albums_response)
        return Response(albums_response)
