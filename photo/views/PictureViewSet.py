from rest_framework.response import Response
from rest_framework import viewsets, mixins, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from ..utils.file import save_file
from ..models import Picture, Tag
from ..serializers import PictureSerializer
from ..utils import PicturePrediction, PictureObjectDetection


class PictureViewSet(mixins.ListModelMixin, mixins.CreateModelMixin, viewsets.ViewSet):
    """

    """
    queryset = Picture.objects.all()
    serializer_class = PictureSerializer

    def list(self, request, *args, **kwargs):
        """

        :param request:
        :return: Response
        """
        pictures = Picture.objects.all()
        if pictures:
            return Response(PictureSerializer(pictures, many=True).data)
        return Response()

    def create(self, request, *args, **kwargs):
        """

        :param request:
        :return: Response
        """
        file = request.FILES['picture']

        if not request.user.photos.filter(name=file.name).exists():
            url = save_file(file)
            picture = request.user.photos.create(name=file.name, url=url, content_type=file.content_type,
                                                 size=file.size, image=url.split('/')[-1])
            picture.save()
            picture.make_predictions()
            # picture.make_detections()

            return Response(PictureSerializer(picture, context={'request': request}).data)
        return Response(status=status.HTTP_409_CONFLICT)
