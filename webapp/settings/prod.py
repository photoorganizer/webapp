from .base import *

DEBUG = False

ADMINS = (
    ('David Luna', 'david@photoorganizer.es'),
)

ALLOWED_HOSTS = ['.photoorganizer.es']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASSWORD'],
        'HOST': os.environ['DB_HOST'],
        'PORT': os.environ['DB_PORT'],
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
            'charset': 'utf8mb4',
        },
        'TEST': {
            'CHARSET': 'utf8mb4',
            'COLLATION': 'utf8mb4_unicode_ci',
        }
    }
}

SECURE_SSL_REDIRECT = True
CSRF_COOKIE_SECURE = True
SECURE_CONTENT_TYPE_NOSNIFF = True  # header to prevent the browser from identifying content types incorrectly
SECURE_BROWSER_XSS_FILTER = True  # header to activate the browser's XSS filtering and help prevent XSS attacks.
SESSION_COOKIE_SECURE = True  # SecureCookie difficult for network traffic sniffers to hijack user sessions
X_FRAME_OPTIONS = 'DENY'
