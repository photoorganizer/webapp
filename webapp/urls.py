from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from .routers import DefaultRouter
from management.urls import router as management_router
from management.views import ObtainTokenView
from dashboard.urls import router as dashboard_router
from photo.urls import router as photo_router

router = DefaultRouter()

router.extend(management_router)
router.extend(dashboard_router)
router.extend(photo_router)

urlpatterns = [
    url(r'api/login/', ObtainTokenView.as_view()),
    path('admin/', admin.site.urls),
    path('api/', include(router.urls))
]
