import os
import dotenv
from django.core.wsgi import get_wsgi_application


DOTENV_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), '.env')

dotenv.read_dotenv(DOTENV_PATH)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'webapp.settings.dev')

application = get_wsgi_application()
